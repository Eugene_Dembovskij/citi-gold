var gulp = require('gulp');
var del = require('del');
var sass = require('gulp-sass');
var util = require('gulp-util');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var tinypng = require('gulp-tinypng-compress');
var cssbeautify = require('gulp-cssbeautify');
var csscomb = require('gulp-csscomb');
var jade = require('gulp-jade');
var jadeInheritance = require('gulp-jade-inheritance');
var rename = require('gulp-rename');
var changed = require('gulp-changed');
var cached = require('gulp-cached');
var gulpif = require('gulp-if');
var filter = require('gulp-filter');
var runSequence = require('run-sequence');
var spritesmith = require('gulp.spritesmith');
var reload = browserSync.reload;
var svgSprite = require('gulp-svg-sprite');
var svg2png = require('gulp-svg2png');
var size = require('gulp-size');
var combineMq = require('gulp-combine-mq');
var sourcemaps = require('gulp-sourcemaps');
var prettify = require('gulp-jsbeautifier');

// configuration

var basePaths = {
	src: 'source/',
	dest: 'html/f/',
};

var paths = {
  source: 'source',
  css: 'source/f/css',
  scss: 'source/f/scss',
  js: 'source/f/js',
  img: 'source/f/i',
  sprites: 'source/f/sprites',
  svg: 'source/f/svg',
  fonts: 'source/f/fonts/**/*',
  content: 'source/content',
  releases: 'releases',
  tpl: 'source/tpl',
  php: 'source/php',
  images: {
		src: basePaths.src + 'f/i/',
		dest: basePaths.dest + 'i/'
	},
	sprite: {
		src: basePaths.src + 'f/sprite/*',
		svg: 'i/sprite.svg',
		css: '../../' + basePaths.src + 'scss/_sprite.scss'
	}
};

watch = {
  jade: paths.source + '/**/*.jade',
  jade_rus: paths.source + '/rus/**/*.jade',
  jade_eng: paths.source + '/eng/**/*.jade',
  css: paths.css + '/**/*.css',
  scss: paths.scss + '/**/*.scss',
  js: paths.js + '/**/*.js',
  html: paths.source + '/**/*.htm',
  img: paths.img + '/**/*',
  php: paths.php + '/**/*.php',
  content: paths.content + '/**/*',
};

dest = {
  source: 'html',
  source_rus: 'html/rus',
  source_eng: 'html/eng',
  css: 'html/f/css',
  scss: 'html/f/css',
  js: 'html/f/js',
  img: 'html/f/i',
  fonts: 'html/f/fonts',
  content: 'html/content',
  php: 'html/php',
  all: 'html/**/*'
};

options = {
  autoprefixer: [
    'last 2 version',
    'safari 5',
    'opera 12.1',
    'ios 6',
    'android 4'
  ],
  imageopt: {
    progressive: true,
    interlaced: true,
    optimizationLevel: 3,
    svgoPlugins: [
      {removeViewBox: false},
      {removeUselessStrokeAndFill: false},
      {cleanupIDs: false}
    ],
    verbose: true,
    use: [pngquant()]
  }
};

devBuild = true;

var changeEvent = function(evt) {
  util.log('File', util.colors.cyan(evt.path.replace(new RegExp('/.*(?=/' + paths.source + ')/'), '')), 'was', util.colors.magenta(evt.type));
};

// browser sync

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: dest.source
    },
    online: false,
    open: false
  });
});

// reloading  -->  .pipe(reload({stream: true}));

// sprites

gulp.task('sprite', function generateSpritesheets () {
  function spriteRetina(retinaName) {
    var retinaName = retinaName || '';
    if (retinaName) {var spacingSize=20} else{var spacingSize=10};
    var spriteData = gulp.src(basePaths.src +'/f/sprites'+retinaName+'/*'+retinaName+'.png')
      .pipe(spritesmith({
        imgName:'sprites'+retinaName+'.png',
        imgPath: '../i/sprites'+retinaName+'.png',
        cssName: '_sprites'+retinaName+'.scss',
        cssFormat:'scss',
        cssOpts: {
          functions: false
        },
        algorithm: 'binary-tree',
        algorithmOpts : {
          sort: false
        },
        padding: spacingSize,
        cssSpritesheetName: 'sprites'+retinaName,
        cssVarMap: function (sprite) {
          sprite.name = 'sprite-' + sprite.name;
        }
      }));
    spriteData.img.pipe(gulp.dest(dest.img));
    spriteData.css.pipe(gulp.dest(paths.scss));
  }
  spriteRetina();
  spriteRetina('_2x');
});

/*gulp.task('svg_sprite', function () {
	return gulp.src(paths.svg+'/*.svg')
		.pipe(svgSprite({
      cssFile: '../scss/_svg_sprite.scss',
      preview: false,
      layout: 'diagonal',
      padding: 5,
      svg: {
        sprite: '../i/sprite.svg'
      },
      templates: {
        css: require("fs").readFileSync(paths.tpl+'/sprite-template.scss', "utf-8")
      }
    }))
    .pipe(gulp.dest(paths.img));
});*/
gulp.task('svgSprite', function () {
	return gulp.src(paths.sprite.src)
		.pipe(svgSprite({
      shape: {
        spacing: {
          padding: 5
        }
      },
      mode: {
        css: {
          dest: "./",
          layout: "diagonal",
          sprite: paths.sprite.svg,
          bust: false,
          render: {
            scss: {
              dest: '../../'+paths.scss+"/_svg_sprite.scss",
              template: paths.tpl+'/sprite-template.scss'
            }
          }
        }
      },
      variables: {
        mapname: "icons"
      }
    }))
    .pipe(gulp.dest(/*basePaths.dest*/'source/f/'));
});

gulp.task('pngSprite', ['svgSprite'], function() {
  return gulp.src('source/f/i/sprite.svg')
    .pipe(svg2png())
    .pipe(size({ showFiles: true }))
    .pipe(gulp.dest(/*paths.images.dest*/'source/f/i/'));
});

gulp.task('svg', ['pngSprite']);

// styles

gulp.task('scss', function() {
  return gulp.src(paths.scss+'/**/*.scss')
    .pipe(gulpif(devBuild, sourcemaps.init({largeFile: true})))
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(autoprefixer(options.autoprefixer))
    .pipe(gulpif(!devBuild, cssbeautify({
      indent: '	',
      autosemicolon: true
    })))
    // .pipe(gulpif(!devBuild, combineMq({ beautify: false })))
    .pipe(gulpif(!devBuild, csscomb()))
    .pipe(gulpif(devBuild, sourcemaps.write('../maps')))
    .pipe(gulp.dest(dest.scss))
    .pipe(size({ title: true }))
    .pipe(reload({stream: true}));
});

gulp.task('css', function() {
  return gulp.src(paths.css+'/**/*.css')
  .pipe(gulp.dest(dest.css))
  .pipe(reload({stream: true}));
});

// scripts

gulp.task('modernizr', function() {
  return gulp.src(paths.js+'/head/modernizr.min.js')
  .pipe(gulp.dest(dest.js));
});

gulp.task('jquery', function() {
  return gulp.src(paths.js+'/head/jquery.min.js')
  .pipe(gulp.dest(dest.js));
});

gulp.task('ie_old', function() {
  return gulp.src(paths.js+'/head/ie_old/*.js')
  .pipe(gulp.dest(dest.js));
});

// gulp.task('fancybox', function() {
//   return gulp.src(paths.js+'/fancybox/**/*.js')
//   .pipe(gulp.dest(dest.js));
// });

gulp.task('vendorjs', function() {
  return gulp.src(paths.js+"/vendor/*.js")
    //.pipe(uglify())
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest(dest.js))
    .pipe(reload({stream: true}));
});

gulp.task('vendorjs-pdf', function() {
  return gulp.src(paths.js+"/vendor-pdf/*.js")
    //.pipe(uglify())
    // .pipe(concat('vendor-pdf.js'))
    .pipe(gulp.dest(dest.js + '/vendor-pdf'))
    .pipe(reload({stream: true}));
});

gulp.task('vendorjs-video', function() {
  return gulp.src(paths.js+"/vendor-video/*.js")
    //.pipe(uglify())
    .pipe(concat('vendor-video.js'))
    .pipe(gulp.dest(dest.js))
    .pipe(reload({stream: true}));
});

gulp.task('userjs', function() {
  return gulp.src(paths.js+"/*.js")
    .pipe(gulp.dest(dest.js))
    .pipe(reload({stream: true}));
});

gulp.task('browserify', function () {
  return browserify(paths.js+'/app.js').bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    //.pipe(uglify())
    .pipe(gulp.dest(dest.js));
});

// images

gulp.task('img', function() {
  return gulp.src(paths.img+'/**/*.*')
  .pipe(gulpif(!devBuild, imagemin([
    imagemin.gifsicle({interlaced: true}),
    imagemin.jpegtran({progressive: true}),
    imagemin.optipng({optimizationLevel: 5}),
    imagemin.svgo({
      plugins: [
        {removeViewBox: true},
        {cleanupIDs: false}
      ]
    })
  ])))
  .pipe(size({ showFiles: true }))
  .pipe(gulp.dest(dest.img))
  .pipe(reload({stream: true}));
});

gulp.task('tinypng', function() {
  return gulp.src(paths.content+'/**/*.{png,jpg,jpeg}')
  .pipe(tinypng({
    key: 'B2JVvsvfoiJV_qbY_Dxa01j5E1qWfqHz'
  }))
  .pipe(gulp.dest(dest.img));
});

// copying statics

gulp.task('fonts', function() {
  return gulp.src(paths.fonts)
  .pipe(gulp.dest(dest.fonts));
});

gulp.task('contents', function() {
  return gulp.src(paths.content+'/**/*.*')
  .pipe(gulp.dest(dest.content))
  .pipe(reload({stream: true}));
});

gulp.task('php', function() {
  return gulp.src(paths.php+'/**/*.*')
  .pipe(gulp.dest(dest.php))
  .pipe(reload({stream: true}));
});

// html

gulp.task('html', function() {
  return gulp.src(paths.source+'/*.htm')
    .pipe(gulp.dest(dest.source))
    .pipe();
});

// jade

gulp.task('jade', function() {
  return gulp.src(paths.source+'/**/!(_)*.jade')
    .pipe(changed(dest.source, {extension: '.htm'}))
    .pipe(gulpif(global.isWatching, cached('jade')))
    .pipe(jadeInheritance({basedir: paths.source}))
    .pipe(gulpif(!devBuild, filter(function (file) {
      return !/\/includes/.test(file.path) && !/includes/.test(file.relative);
      // return !/\/_/.test(file.path) && !/^_/.test(file.relative);
    })))
    .pipe(jade())
    .pipe(prettify({
      braceStyle: 'expand',
      indentWithTabs: true,
      indentInnerHtml: true,
      preserveNewlines: true,
      endWithNewline: true,
      wrapLineLength: 120,
      maxPreserveNewlines: 50,
      wrapAttributesIndentSize: 1,
      unformatted: ['use']
    }))
    .pipe(rename({
      extname: ".htm"
    }))
    .pipe(gulp.dest(dest.source))
    .on('end', browserSync.reload);
});

gulp.task('copy-redirect', function(){
	gulp.src(paths.source + '/index.html')
		.pipe(gulp.dest(dest.source));
});

// clean

gulp.task('clean', function() {
  return del([
    dest.source,
    paths.releases
  ]);
});

// watch

gulp.task('setWatch', function() {
  global.isWatching = true;
});

gulp.task('watch', ['setWatch', 'browser-sync'], function() {
  gulp.watch(watch.css, ['css']);
  gulp.watch(watch.scss, ['scss']);
  gulp.watch(watch.js, ['vendorjs', 'userjs', 'vendorjs-pdf', 'vendorjs-video']);
  gulp.watch(watch.img, ['img']);
  gulp.watch(watch.fonts, ['fonts']);
  gulp.watch(watch.jade, ['jade']);
  //gulp.watch(watch.html, ['html']);
  gulp.watch(watch.content, ['contents']);
  gulp.watch(watch.php, ['php']);
});

// build

gulp.task('build', ['clean'], function() {
  runSequence(
    'sprite', 'svg', 'contents',
    ['css', 'scss', 'vendorjs', 'userjs', 'vendorjs-pdf', 'vendorjs-video', 'jquery', 'modernizr', 'ie_old',  /*'fancybox',*/ 'img', 'fonts', /*'html',*/ 'jade', 'php', 'copy-redirect'],
    'watch', function() {}
  );
});

// default

gulp.task('default', ['build']);

gulp.task('deploy', ['clean'], function() {
  devBuild = false;
  runSequence(
    'sprite', 'svg', 'contents',
    ['css', 'scss', 'vendorjs', 'userjs', 'vendorjs-pdf', 'vendorjs-video', 'jquery', 'modernizr', 'ie_old',  /*'fancybox',*/ 'img', 'fonts', /*'html',*/ 'jade', /*'tinypng',*/ 'php', 'copy-redirect']
  );
});
