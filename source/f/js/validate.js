Form = {
	branches: null,
	init: function(){
		var phone = $('#cglead input[name="phonenum11"]');
		if (phone.exists()) {
			phone.mask('+9(999)999-99-99');
		}
		
		branches = {
				1 : [
					{
						"name": "Метрополис",
						"value": "Отделение 'Метрополис'"
					},
					{
						"name": "На Арбатe",
						"value": "Отделение 'На Арбатe'"
					},
					{
						"name": "На Большой Никитской",
						"value": "Отделение 'На Большой Никитской'"
					},
					{
						"name": "На Комсомольском проспекте",
						"value": "Отделение 'На Комсомольском проспекте'"
					},
					{
						"name": "На Красной Пресне",
						"value": "Отделение 'На Красной Пресне'"
					},
					{
						"name": "На Ленинском",
						"value": "Отделение 'На Ленинском'"
					},
					{
						"name": "На Марксистской",
						"value": "Отделение 'На Марксистской'"
					},
					{
						"name": "На Тверской-Ямской",
						"value": "Отделение 'На Тверской-Ямской'"
					},
					{
						"name": "Остоженка",
						"value": "Отделение 'Остоженка'"
					},
					{
						"name": "Павелецкое",
						"value": "Отделение 'Павелецкое'"
					}
				],
				2 : [
					{
						"name": "Большой проспект",
						"value": "Отделение 'Большой проспект (СПб)'"
					},
					{
						"name": "Московский проспект",
						"value": "Отделение 'Московский проспект (СПб)'"
					},
					{
						"name": "Невский проспект",
						"value": "Отделение 'Невский проспект (СПб)'"
					}
				],
				3 : [
					{
						"name": "Волгоградский",
						"value": "Отделение 'Волгоградский (Волгоград)'"
					}
				],
				4 : [
					{
						"name": "Уральский",
						"value": "Отделение 'Уральский (Екатеринбург)'"
					}
				],
				5: [
					{
						"name": "Приволжский",
						"value": "Отделение 'Приволжский (Нижний Новгород)'"
					}
				],
				6: [
					{
						"name": "Донской",
						"value": "Отделение 'Донской (Ростов-на-Дону)'"
					}
				],
				7: [
					{
						"name": "Средневолжский",
						"value": "Отделение 'Средневолжский (Самара)'"
					}
				],
				8: [
					{
						"name": "Башкортостан",
						"value": "Отделение 'Башкортостан (Уфа)'"
					}
				]
			};
		
		if($('.select-box').exists()){
			$(".select-box").selecter({
				customClass: 'scroll-select',
				handleSize: 60,
				callback: selectCallback,
				mobile: true
			});
		}
		
		function selectCallback(val, index){
			if($(this).find('select').attr('name') == 'city'){
				if($(this).find('select').val() != ''){
					$('#branch').closest('.selecter').find('.selecter-options').scroller('destroy');
					$('#branch').empty().append($("<option selected='selected' value=''>Отделение банка</option>")).val('');
					$.each(branches[index], function(key, text){
						$('#branch').append($("<option></option>").attr("value", text.value).text(text.name));
					});
					$("#branch").selecter("update");
					$('#branch').closest('.selecter').find('.selecter-options').scroller();
					$("#branch").selecter("enable");
				} else {
					$('#branch').empty().append($("<option selected='selected' value=''>Отделение банка</option>")).val('');
					$("#branch").selecter("update");
					$('#branch').closest('.selecter').find('.selecter-options').scroller('destroy');
					$('#branch').closest('.selecter').find('.selecter-options').scroller();
					$("#branch").selecter("disable");
				}
			}
		}
	}
};

FormVal = {
	$Form: null,
	FocusValue: 1,
	params: {},
	validation: function(e) {
		var $this = $(e.currentTarget);
		var data = '';
		var isError = false;
		if (e.data.jForm) {
			FormVal.$Form = e.data.jForm;
		}
		var tmp, tmpName;
		if(!$('#cglead input[name="agree"]').prop('checked') && $this.hasClass('disabled')) return false;
		FormVal.$Form.find('.js-requere').each(function(){
			tmp = $(this);
			tmpName = tmp.attr('name');
			if(tmpName){
				FormVal.params[tmpName] = tmp.val().trim();
			}
			
			if(tmpName == 'name'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					if (tmp.val() == '	') {
						tmp.val() = '';
					}
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				if (tmp.val().length < 2) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}

				if (!Utilities.TextValidateWords(tmp.val())) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'phonenum11'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'city'){
				if(tmp.val() == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'branch' && !(tmp.parent().hasClass('disabled'))){
				if(tmp.val() == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'comment'){
				/*if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}*/
				if(Utilities.trimSpace(tmp.val()) != ''){
					if (tmp.val().length < 2) {
						if (tmp.val() == '	') {
							tmp.val() = '';
						}
						
						tmp.parent().addClass('has-error');
						isError = true;
						return false;
					}
					if (!Utilities.textValidateArea(tmp.val())) {
						tmp.parent().addClass('has-error');
						isError = true;
						return false;
					}
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'client'){
				if (!tmp.prop("checked")) {
					tmp.val('не клиент');
					FormVal.params[tmpName] = tmp.val();
				} else {
					tmp.val('клиент банка');
					FormVal.params[tmpName] = tmp.val();
				}
			}
			
			if(tmpName == 'agree'){
				if (!tmp.prop("checked")) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.val('1');
				tmp.parent().removeClass('has-error');
				isError = false;
			}
		});

		// isError = true;

		if(isError){
			FormVal.$Form.find('.has-error').each(function(){
				if($(this).hasClass('has-error')){
					$('html').animate({ scrollTop: parseInt($(this).find('.js-requere').offset().top)}, 'slow');
					return false;
				}
			});
		} else {
			var data = { Fname: FormVal.params.name, tel_num_1: FormVal.params.phonenum11, city: FormVal.params.city, branches: FormVal.params.branch, client: FormVal.params.client, comment: FormVal.params.comment, sourcecode: $('#sourcecode').val(), IEWAcategory: $('#IEWAcategory').val(), utm_source: $('#utm_source').val(), utm_term: $('#utm_term').val(), icid: $('#icid').val() };
			var path = window.location.pathname.split("/");

			function eventSendForm() {
				var loc = encodeURIComponent(window.location.href);
				var ref = encodeURIComponent(document.referrer);
				var pathLoc = loc.split("%2F");
				var matches = pathLoc[pathLoc.length - 1].substr(0, pathLoc[pathLoc.length - 1].search(/\.htm/i));
				var img = document.createElement('img');

				img.width = 1;
				img.height = 1;
				img.style.display = 'none';
				if (matches === 'servis-i-privilegii') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=costep_2&__location=' + loc + '&__referrer=' + ref;
				}
				if (matches === 'privileges') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=castep_0&__location=' + loc + '&__referrer=' + ref;
				}
				// if (matches === 'citigold_client') {
				// 	img.src = '//tag.rutarget.ru/tag?event=castep_1&__location=' + loc + '&__referrer=' + ref;
				// }
				img.alt = '';
				document.body.insertBefore(img, document.body.firstChild);
			}

			function eventSuccessForm() {
				var loc = encodeURIComponent(window.location.href);
				var ref = encodeURIComponent(document.referrer);
				var pathLoc = loc.split("%2F");
				var matches = pathLoc[pathLoc.length - 1].substr(0, pathLoc[pathLoc.length - 1].search(/\.htm/i));
				var img = document.createElement('img');

				img.width = 1;
				img.height = 1;
				img.style.display = 'none';
				if (matches === 'servis-i-privilegii') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=order_consult&__location=' + loc + '&__referrer=' + ref;
				}
				
				if (matches === 'privileges') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=order_card&__location=' + loc + '&__referrer=' + ref;
				}
				if (matches === 'citigold_client') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=order_card&__location=' + loc + '&__referrer=' + ref;
				}
				img.alt = '';
				document.body.insertBefore(img, document.body.firstChild);
			}
			eventSendForm();
			eventSuccessForm();
			//console.log('data= ', data);
			// $.post(FormVal.$Form.attr('action'), data, function(json){
			// 	//if(json.success){}
			// }, "json");
			Popup.shownModal('#popup-success');
			if(path.length > 1){
				//var matches = path[path.length - 1].match(/\b\w+?\b/);
				//analitics('event13', matches[0]);

				// analitics('event13', ';Citigold', '1464333941135', '1464333941135');
				// if (FormVal.$Form.attr('name') == 'cglead-1') {
				// 	yaCounter10209925.reachGoal('Request_a_call');
				// 	ga('send', 'event', 'Click', 'Form', 'Send_Form_Request_a_call');
				// 	eventSuccessForm();
				// } else {
				// 	yaCounter10209925.reachGoal('Form_citigold_client');
				// 	ga('send', 'event', 'Click', 'Form', 'citigold_client');
				// }
			}
			//$('#popup-feedback .close').trigger('click');
			FormVal.clear(FormVal.$Form);
			// window.location.href = "https://www.citibank.ru/russia/main/rus/citigold_client_thankyou.html";
		}
	},
	
	clear: function(idForm){
		$(idForm).find('.has-error').removeClass('has-error');
		$(idForm).find('.error-text').empty().hide().html('<li></li>');
		$(idForm)[0].reset();
		$('#city').val('').trigger('change');
		$('#submit').addClass('disabled');
	},
	
	init: function() {
		$('.form-field.required .field-email').on('blur', function(e) {
			var regexp = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
			if (regexp.test(this.value) !== true) {
				$(this).parent().addClass('field_error');
			} else {
				$(this).parent().removeClass('field_error');
			}
		});

		$('.form-field.required .field-text').on('keyup', function(e) {
			var regexp = /^[a-zA-Zа-яА-Я@]+$/;
			if (regexp.test(this.value) !== true) {
				this.value = this.value.replace(/[^a-zA-Zа-яА-Я@]+/, '');
			}
		});
		
		$('#cglead input[name="agree"]').on('change', function(){
			$('#submit').toggleClass('disabled');
		});

		$('#submit').on('click', { jForm: $('#cglead')}, FormVal.validation);
		/*$('#submit').on('click', function(){
			$('#popup-feedback .close').trigger('click');
			Popup.shownModal('#popup-success');
		});*/
	}
};

FormValTwo = {
	$Form: null,
	FocusValue: 1,
	params: {},
	validation: function(e) {
		var $this = $(e.currentTarget);
		var data = '';
		var isError = false;
		if (e.data.jForm) {
			FormValTwo.$Form = e.data.jForm;
		}
		var tmp, tmpName;
		if(!$('#cglead_1 input[name="agree"]').prop('checked') && $this.hasClass('disabled')) return false;
		FormValTwo.$Form.find('.js-requere').each(function(){
			tmp = $(this);
			tmpName = tmp.attr('name');
			if(tmpName){
				FormValTwo.params[tmpName] = tmp.val().trim();
			}
			
			if(tmpName == 'name_1'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					if (tmp.val() == '	') {
						tmp.val() = '';
					}
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				if (tmp.val().length < 2) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}

				if (!Utilities.TextValidateWords(tmp.val())) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'phonenum11_1'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'city_1'){
				if(tmp.val() == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'client_1'){
				if (!tmp.prop("checked")) {
					tmp.val('не клиент');
					FormValTwo.params[tmpName] = tmp.val();
				} else {
					tmp.val('клиент банка');
					FormValTwo.params[tmpName] = tmp.val();
				}
			}
			
			if(tmpName == 'agree_1'){
				if (!tmp.prop("checked")) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.val('1');
				tmp.parent().removeClass('has-error');
				isError = false;
			}
		});
		
		if(isError){
			FormValTwo.$Form.find('.has-error').each(function(){
				if($(this).hasClass('has-error')){
					$('html').animate({ scrollTop: parseInt($(this).find('.js-requere').offset().top)}, 'slow');
					return false;
				}
			});
		} else {
			var data = { Fname: FormValTwo.params.name_1, tel_num_1: FormValTwo.params.phonenum11_1, city: FormValTwo.params.city_1, client: FormValTwo.params.client_1, sourcecode: $('#sourcecode_1').val(), IEWAcategory: $('#IEWAcategory_1').val(), utm_source: $('#utm_source').val(), utm_term: $('#utm_term').val(), icid: $('#icid').val() };
			var path = window.location.pathname.split("/");

			function eventSendForm() {
				var loc = encodeURIComponent(window.location.href);
				var ref = encodeURIComponent(document.referrer);
				var pathLoc = loc.split("%2F");
				var matches = pathLoc[pathLoc.length - 1].substr(0, pathLoc[pathLoc.length - 1].search(/\.htm/i));
				var img = document.createElement('img');

				img.width = 1;
				img.height = 1;
				img.style.display = 'none';
				if (matches === 'servis-i-privilegii') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=costep_2&__location=' + loc + '&__referrer=' + ref;
				}
				if (matches === 'privileges') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=castep_0&__location=' + loc + '&__referrer=' + ref;
				}
				if (matches === 'citigold_client') {
					img.src = '//tag.rutarget.ru/tag?event=castep_1&__location=' + loc + '&__referrer=' + ref;
				}
				img.alt = '';
				document.body.insertBefore(img, document.body.firstChild);
			}

			function eventSuccessForm() {
				var loc = encodeURIComponent(window.location.href);
				var ref = encodeURIComponent(document.referrer);
				var pathLoc = loc.split("%2F");
				var matches = pathLoc[pathLoc.length - 1].substr(0, pathLoc[pathLoc.length - 1].search(/\.htm/i));
				var img = document.createElement('img');

				img.width = 1;
				img.height = 1;
				img.style.display = 'none';
				if (matches === 'servis-i-privilegii') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=order_consult&__location=' + loc + '&__referrer=' + ref;
				}
				
				if (matches === 'privileges') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=order_card&__location=' + loc + '&__referrer=' + ref;
				}
				img.alt = '';
				document.body.insertBefore(img, document.body.firstChild);
			}

			eventSendForm();
			
			// console.log('data= ', data);
			$.post(FormValTwo.$Form.attr('action'), data, function(json){
				//if(json.success){}
			}, "json");
			Popup.shownModal('#popup-success');
			if(path.length > 1){
				eventSuccessForm();
				//var matches = path[path.length - 1].match(/\b\w+?\b/);
				//analitics('event13', matches[0]);
				analitics('event13', ';Citigold', '1464333941135', '1464333941135');
				yaCounter10209925.reachGoal('Request_a_call');
				ga('send', 'event', 'Click', 'Form', 'Send_Form_Request_a_call');
			}
			//$('#popup-feedback .close').trigger('click');
			FormValTwo.clear(FormValTwo.$Form);
			// window.location.href = "https://www.citibank.ru/russia/main/rus/citigold_client_thankyou.html";
		}
	},
	
	clear: function(idForm){
		$(idForm).find('.has-error').removeClass('has-error');
		$(idForm).find('.error-text').empty().hide().html('<li></li>');
		$(idForm)[0].reset();
		$('#city_1').val('').trigger('change');
		$('#submit_1').addClass('disabled');
	},
	
	init: function() {
		var phone = $('#cglead_1 input[name="phonenum11_1"]');
		if (phone.exists()) {
			phone.mask('+9(999)999-99-99');
		}
		$('.form-field.required .field-email').on('blur', function(e) {
			var regexp = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
			if (regexp.test(this.value) !== true) {
				$(this).parent().addClass('field_error');
			} else {
				$(this).parent().removeClass('field_error');
			}
		});

		$('.form-field.required .field-text').on('keyup', function(e) {
			var regexp = /^[a-zA-Zа-яА-Я@]+$/;
			if (regexp.test(this.value) !== true) {
				this.value = this.value.replace(/[^a-zA-Zа-яА-Я@]+/, '');
			}
		});
		
		$('#cglead_1 input[name="agree"]').on('change', function(){
			$('#submit_1').toggleClass('disabled');
		});

		
		$('#submit_1').on('click', { jForm: $('#cglead_1')}, FormValTwo.validation);
		/*$('#submit').on('click', function(){
			$('#popup-feedback .close').trigger('click');
			Popup.shownModal('#popup-success');
		});*/
	}
};

FormValFriend = {
	$Form: null,
	FocusValue: 1,
	params: {},
	validation: function(e) {
		var $this = $(e.currentTarget);
		var data = '';
		var isError = false;
		if (e.data.jForm) {
			FormValFriend.$Form = e.data.jForm;
		}
		var tmp, tmpName;
		if(!$('#cgmgm input[name="agree_1"]').prop('checked') && !$('#cgmgm input[name="agree_2"]').prop('checked') && $this.hasClass('disabled')) return false;
		FormValFriend.$Form.find('.js-requere').each(function(){
			tmp = $(this);
			tmpName = tmp.attr('name');
			if(tmpName){
				FormValFriend.params[tmpName] = tmp.val().trim();
			}
			
			if(tmpName == 'name'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					if (tmp.val() == '	') {
						tmp.val() = '';
					}
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				if (tmp.val().length < 2) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}

				if (!Utilities.TextValidateWords(tmp.val())) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'card_number'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'nameRec'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					if (tmp.val() == '	') {
						tmp.val() = '';
					}
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				if (tmp.val().length < 2) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'email'){
				var regexp = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
				if (!Utilities.trimSpace(tmp.val()) == '' && regexp.test(tmp.val()) !== true) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'phonenum10'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}

			if(tmpName == 'phonenum11'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'comment'){
				/*if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}*/
				if(Utilities.trimSpace(tmp.val()) != ''){
					if (tmp.val().length < 2) {
						if (tmp.val() == '	') {
							tmp.val() = '';
						}
						
						tmp.parent().addClass('has-error');
						isError = true;
						return false;
					}
					if (!Utilities.textValidateArea(tmp.val())) {
						tmp.parent().addClass('has-error');
						isError = true;
						return false;
					}
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'agree_1'){
				if (!tmp.prop("checked")) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.val('1');
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'agree_2'){
				if (!tmp.prop("checked")) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.val('1');
				tmp.parent().removeClass('has-error');
				isError = false;
			}
		});
		
		if(isError){
			FormValFriend.$Form.find('.has-error').each(function(){
				if($(this).hasClass('has-error')){
					if ($('.modal.fade.in').is(':visible')) {
						$('.modal.fade.in').animate({ scrollTop: parseInt($(this).find('.js-requere').offset().top) - parseInt($(this).parents('.modal-dialog').offset().top) }, 'slow');
						return false;
					} else {
						$('html, body').animate({ scrollTop: parseInt($(this).find('.js-requere').offset().top)}, 'slow');
						return false;
					}
				}
			});
		} else {
			var data = {name: FormValFriend.params.name, phonenum11: FormValFriend.params.phonenum11, nameRec: FormValFriend.params.nameRec, email: FormValFriend.params.email, comment: FormValFriend.params.comment, sourcecode: $('#sourcecode_rec').val()};
			var path = window.location.pathname.split("/");
			
			//console.log('data= ', data);
			$.post(FormValFriend.$Form.attr('action'), data, function(json){
				//if(json.success){}
			}, "json");
			Popup.shownModal('#popup-guid-success');
			if(path.length > 1){
				//var matches = path[path.length - 1].match(/\b\w+?\b/);
				//analitics('event13', matches[0]);
				analitics('event13', ';Citigold', '1464333941135', '1464333941135');
				yaCounter10209925.reachGoal('Specials_Recommend_a_friend');
				ga('send', 'event', 'Click', 'Form', 'Specials_Recommend_a_friend');
			}
			$('#popup-guid .close').trigger('click');
			FormValFriend.clear(FormValFriend.$Form);
		}
	},
	
	clear: function(idForm){
		$(idForm).find('.has-error').removeClass('has-error');
		$(idForm).find('.error-text').empty().hide().html('<li></li>');
		$(idForm)[0].reset();
		$('#submit-rec').addClass('disabled');
	},
	
	init: function() {
		var cardNumber = $('#cgmgm input[name="card_number"]');
		var phone = $('#cgmgm input[name="phonenum11"]');
		if (cardNumber.exists()) {
			cardNumber.mask('9999 9999 9999 9999');
		}
		if (phone.exists()) {
			phone.mask('+9(999)999-99-99');
		}
		
		$('.field-email.js-requere').on('blur', function(e) {
			var regexp = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
			if (!Utilities.trimSpace(this.value) == '' && regexp.test(this.value) !== true) {
				$(this).parent().addClass('has-error');
			} else {
				$(this).parent().removeClass('has-error');
			}
		});

		$('.form-field.required .field-text').on('keyup', function(e) {
			var regexp = /^[a-zA-Zа-яА-Я@]+$/;
			if (regexp.test(this.value) !== true) {
				this.value = this.value.replace(/[^a-zA-Zа-яА-Я@]+/, '');
			}
		});
		
		$('#cgmgm input[name="agree_1"], #cgmgm input[name="agree_2"]').on('change', function(){
			$('#submit-rec').addClass('disabled');
			if($('#cgmgm input[name="agree_1"]').prop('checked') && $('#cgmgm input[name="agree_2"]').prop('checked')){
				$('#submit-rec').removeClass('disabled');
			}
		});

		$('#submit-rec').on('click', { jForm: $('#cgmgm')}, FormValFriend.validation);
	}
};

FormValElib = {
	$Form: null,
	FocusValue: 1,
	params: {},
	data: {},
	validation: function(e) {
		var $this = $(e.currentTarget);
		var data = '';

		if (e.data.jForm) {
			FormValElib.$Form = e.data.jForm;
		}
		var tmp, tmpName;
		FormValElib.$Form.find('.js-requere').each(function(){
			tmp = $(this);
			tmpName = tmp.attr('name');
			if(tmp.prop('checked')){
				FormValElib.params[tmpName] = tmp.val();
			}
		});
		
		var data = {elib: FormValElib.params.elib_vote, sourcecode: $('#sourcecode_elib').val(), customer_id: $('#customer_id').val()};
		var path = window.location.pathname.split("/");
		$.post(FormValElib.$Form.attr('action'), data, function(json){
			//if(json.success){}
		}, "json");

		Tools.setCookie('elibform', 'passed');
		
		if(path.length > 1){
			//var matches = path[path.length - 1].match(/\b\w+?\b/);
			//analitics('event13', matches[0]);
			analitics('event13', ';Citigold', '1464333941135', '1464333941135');
		}
		
		FormValElib.clear(FormValElib.$Form);
	},
	
	complete: function(data, el){
		var path = window.location.pathname.split("/");
		$.post($('#elib').attr('action'), data, function(json){
			//if(json.success){}
		}, "json");

		Tools.setCookie('elibform', 'passed');

		if(path.length > 1){
			//var matches = path[path.length - 1].match(/\b\w+?\b/);
			//analitics('event13', matches[0]);
			analitics('event13', ';Citigold', '1464333941135', '1464333941135');
		}

		var $modal = $(el).children('.modal-dialog');
		$('body').addClass('modal-open');
		$(el).collapse('show').add($modal.css({
			"top": (($(window).height() - $modal.outerHeight()) / 2 < 10) ? '10px' : ($(window).height() - $modal.outerHeight()) / 2 + 'px',
			"left": '0',
			"right": '0',
			"position": 'absolute',
			"z-index": '1020'
		})).before('<div class="modal-backdrop fade"></div>').add($('.modal-backdrop').css({
			'background-color': '#ffffff',
			'opacity': '1'
		}).addClass('in').css('height', '110%'));

		if(/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
			$('.modal').addClass('modal_for_ios');
			$('.modal-backdrop').addClass('modal_for_ios');
		}
		
		$(el  + ' .close').on('click', function(){
			window.location.href='https://www.citibank.ru/russia/citigold/rus/citigold_ru.htm';
		});
		$(el + ' .modal-backdrop').on('click', function(){
			return false;
		});
	},
	
	clear: function(idForm){
		$(idForm)[0].reset();
		Popup.shownModalFixed('#popup-elib');
		$('#popup-elib .close').on('click', function(){
			$('.form-emoji').remove();
		});
	},
	
	init: function() {
		$('.form-emoji__item').on('change', { jForm: $('#elib')}, FormValElib.validation);
		var userType = Tools.getCookie('elibform');
		if (userType == 'passed') {
			$('.form-emoji').remove();
		}

		$(window).load(function() {
			var url = undefined,
				tmp = [];
			
			if(location.hash.substr(1).split("?").length > 1){
				url = Tools.findInUrlParam();

				url.split("&").forEach(function(item){
					tmp = item.split("=");
					if(tmp[0] == 'utm_source'){
						FormValElib.data.sourcecode = tmp[1];
					}
					if(tmp[0] == 'utm_elib'){
						FormValElib.data.elib = tmp[1]
					}
				});
				
				if(Tools.objectSize(FormValElib.data))
					FormValElib.complete(FormValElib.data, '#popup-elib');
			}
		});
	}
};

$(function() {
	Form.init();
	FormVal.init();
	FormValTwo.init();
	FormValFriend.init();
	// FormValElib.init();
});
