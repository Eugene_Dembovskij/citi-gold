FormRequest = {
	$Form: null,
	FocusValue: 1,
	params: {},
	validation: function(e) {
		var $this = $(e.currentTarget);
		var data = '';
		var isError = false;
		if (e.data.jForm) {
			FormRequest.$Form = e.data.jForm;
		}
		var tmp, tmpName;
		if(!FormRequest.$Form.find('input[name="agree_1"]').prop('checked') && $this.hasClass('disabled')) return false;
		if(!FormRequest.$Form.find('input[name="agree_2"]').prop('checked') && $this.hasClass('disabled')) return false;

		FormRequest.$Form.find('.js-requere').each(function(){
			tmp = $(this);
			tmpName = tmp.attr('name');
			if(tmpName){
				FormRequest.params[tmpName] = tmp.val();
			}
			
			if(tmpName == 'name_1' || tmpName == 'name_2'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					if (tmp.val() == '	') {
						tmp.val() = '';
					}
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				if (tmp.val().length < 2) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'phonenum11_1' || tmpName == 'phonenum11_2'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'city_1' || tmpName == 'city_2'){
				if(tmp.val() == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'agree_1' || tmpName == 'agree_2'){
				if (!tmp.prop("checked")) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.val('1');
				tmp.parent().removeClass('has-error');
				isError = false;
			}
		});
		
		if(isError){
			FormRequest.$Form.find('.has-error').each(function(){
				if($(this).hasClass('has-error')){
					$('html').animate({ scrollTop: parseInt($(this).find('.js-requere').offset().top)}, 'slow');
					return false;
				}
			});
		} else {
			var idForm = $(FormRequest.$Form).attr('id').substr(-1);
			var data = {Fname: FormRequest.params["name_" + idForm], tel_num_1: FormRequest.params["phonenum11_" + idForm], city: FormRequest.params["city_" + idForm], sourcecode: $('#sourcecode').val(), IEWAcategory: "CitiGold Lead Form"};

			var path = window.location.pathname.split("/");
			
			$.post(FormRequest.$Form.attr('action'), data, function(json){
			}, "json");

			// if(path.length > 1){
			// 	analitics('event12', ';Citigold', '1464333941135', '1464333941135');
			// 	yaCounter10209925.reachGoal('Form_citigold_client');
			// 	ga('send', 'event', 'Click', 'Form', 'citigold_client');
			// }
			FormRequest.clear(FormRequest.$Form);
			Popup.shownModal('#popup-request');
			FormRequest.runEvent();
		}
	},
	
	clear: function(idForm){
		$(idForm).find('.has-error').removeClass('has-error');
		$(idForm).find('.error-text').empty().hide().html('<li></li>');
		$(idForm)[0].reset();
		$(idForm).find('#city_1').val('').trigger('change');
		$(idForm).find('#city_2').val('').trigger('change');
		$(idForm).find('#submit_1').addClass('disabled');
		$(idForm).find('#submit+2').addClass('disabled');
	},
	
	runEvent: function() {
		s.events="event13";
		var s_code = s.t();
		if (s_code) {
			document.write(s_code);
		};
		ga('send', 'event', 'Сlick', 'Form', 'Citigold_Form_onlain_zajavka');
		yaCounter10209925.reachGoal('Citigold_Form_onlain_zajavka');
	},

	
	init: function() {
		var phone_1 = $('#cgclient_1 input[name="phonenum11_1"]'),
				phone_2 = $('#cgclient_2 input[name="phonenum11_2"]');
		if (phone_1.length){
			phone_1.mask('+9(999)999-99-99');
		}
		if (phone_2.length){
			phone_2.mask('+9(999)999-99-99');
		}

		$('#cgclient_1 input[name="agree_1"]').on('change', function(){
			$('#cgclient_1 #submit_1').toggleClass('disabled');
		});

		$('#cgclient_2 input[name="agree_2"]').on('change', function(){
			$('#cgclient_2 #submit_2').toggleClass('disabled');
		});

		
		$('#submit_1').on('click', { jForm: $('#cgclient_1')}, FormRequest.validation);
		$('#submit_2').on('click', { jForm: $('#cgclient_2')}, FormRequest.validation);
	}
};

$(function() {
	FormRequest.init();
});
