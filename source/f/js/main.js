// Google Analytics tracking code
if ((window.location.href.indexOf('citibank.ru')>=0) || (window.location.href.indexOf('citibank.com')>=0)) {
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-17370299-1']);
	_gaq.push(['_setDomainName', 'none']);
	_gaq.push(['_setAllowLinker', true]);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
}

// Yandex.Metrika tracking code
if ((window.location.href.indexOf('citibank.ru')>=0) || (window.location.href.indexOf('citibank.com')>=0)) {
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter10209925 = new Ya.Metrika({id:10209925,
				webvisor:true,
				clickmap:true,
				accurateTrackBounce:true});
			} catch(e) { }
		});
	
		var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
	
		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
}

jQuery.fn.exists = function() {
	return this.length > 0;
};
// to align the elements height
// container .box
// elements .box-img and .box-content
// call $(element).box();
// call $(element).box('refresh');
+function ($) {
	'use strict';
	
	var self;
	
	var Box = function(element, options) {
		self = this;
		self.options = '';
		self.$element = null;
		self.isShown = false;
		self.init('box', element, options);
		self.windowWidth = $(window).width();
		self.windowHeight = $(window).height();
	};
	
	Box.prototype.init = function(type, element, options) {
		self.$element = $(element);
		var $elementChild = self.$element.find('.box');
		
		this.addArray($elementChild, self.$element);
	};
	
	Box.prototype.addArray = function(element, parent) {
		var elementArrayTmp = [],
				elementArray = [];
		var $items = element,
				$itemsImg = $items.children('.box-img'),
				$itemsContent = $items.children('.box-content');
		
		self.isShown = true;
		self.destroy($items);

		var i = 0,
				j = 0,
				len = $items.length;
		for (i; i < len; i++) {
			if($itemsImg.length !== 0){
				if($itemsImg[i].offsetHeight !== 0)
					elementArrayTmp.push($itemsImg[i].offsetHeight);
			}
			elementArray.push($itemsContent[i].offsetHeight);
		}
		
		$items.each(function(key, element) {
			if($(window).width() <= 760) return false;
			$(this).css('height', elementArray.max() + elementArrayTmp.min() + 'px');
			$itemsImg.css({
				'height': elementArrayTmp.max() + 'px',
				'overflow': 'hidden'
			});
			$itemsContent.css({
				'height': (elementArray.max()) + 'px',
				'transition': 'all 0.3s'
			});
		});
		
		self.resize();
	};
	
	Box.prototype.destroy = function(element) {
		var elem = (element) ? element : self.$element.find('.box');
		var $items = elem,
				$itemsImg = $items.children('.box-img'),
				$itemsContent = $items.children('.box-content');
		$items.removeAttr('style');
		$itemsImg.removeAttr('style');
		$itemsContent.removeAttr('style');
	};
	
	Box.prototype.refresh = function() {
		var triggers = self.$element.attr('class').split(' ');
		var parent = $('.' + triggers[triggers.length - 1]);
		$.each(parent, function(){
			if($(this).is(':visible')){
				var $elementChild = $(this).find('.box');
				self.addArray($elementChild, this);
			}
		});
	};
	
	Box.prototype.resize = function () {
		if (self.isShown) {
			$(window).on('resize.bs.box', $.proxy(self.handleUpdate, this));
		} else {
			$(window).off('resize.bs.box');
		}
	};
	
	Box.prototype.handleUpdate = function(element) {
		var windowWidthNew = $(window).width();
		var windowHeightNew = $(window).height();
		if(self.windowWidth != windowWidthNew || self.windowHeight != windowHeightNew){
			self.windowWidth = windowWidthNew;
			self.windowHeight = windowHeightNew;
			self.refresh.call();
		}
	};
	
	Array.prototype.max = function() {
		return Math.max.apply(null, this);
	};

	Array.prototype.min = function() {
		return Math.min.apply(null, this);
	};

	function Plugin(option) {
		return this.each(function() {
			var $this = $(this);
			var data = $this.data('bs.box');
			var options = typeof option == 'object' && option;
			
			if($(this).is(':visible')){
				if (!data) $this.data('bs.box', (data = new Box(this, options)));
				if (typeof option == 'string') data[option]();
			}
		});
	}

	$.fn.box = Plugin;
	$.fn.box.Constructor = Box;

}(jQuery);

var slider = false;

var Utilities = {
	mobile: function() {
		var check = false;
		if (/Android|webOS|iPhone|iPod|iPad|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			check = true;
		} else {
			check = false;
		}
		return check;
	},
	
	trimSpace: function(x) {
		var emptySpace = / /g;
		var trimAfter = x.replace(emptySpace, "");
		return (trimAfter);
	},
	NumberValidate: function(incomingString, defaultValue) {
		if (this.trimSpace(incomingString).length === 0 || incomingString.search(/[^0-9]/g) != -1) {
			return false;
		} else {
			return true;
		}
	},
	
	TextValidate: function(incomingString, defaultValue) {
		if (this.trimSpace(incomingString).length === 0 || incomingString.search(/[^а-яА-ЯёЁ]/g) != -1) {
			return false;
		} else {

			return true;
		}
	},

	TextValidateWords: function(incomingString, defaultValue) {
		var str = incomingString.trim();
		var regexp = /^(?!.*  )[а-яА-ЯёЁ ]*$/g;
		if (this.trimSpace(incomingString).length === 0 || !regexp.test(str)) {
			return false;
		} else {
			return true;
		}
	},
	
	textValidateArea: function(incomingString, defaultValue) {
		if (this.trimSpace(incomingString).length === 0 || incomingString.search(/[^а-я А-ЯёЁ!#%^/.:;0-9-\n/?{}&*(,)+=_~`|\[\]]/g) != -1) {
			return false;
		} else {
			return true;
		}
	},

	textValidateAreaEng: function(incomingString, defaultValue) {
		if (this.trimSpace(incomingString).length === 0 || incomingString.search(/[^a-z A-Z!#%^/.:;0-9-\n/?{}&*(,)+=_~`|\[\]]/g) != -1) {
			return false;
		} else {
			return true;
		}
	},

	TextValidateWordsEng: function(incomingString, defaultValue) {
		var str = incomingString.trim();
		var regexp = /^(?!.*  )[a-z A-Z ]*$/g;
		if (this.trimSpace(incomingString).length === 0 || !regexp.test(str)) {
			return false;
		} else {
			return true;
		}
	}
};

var touchMenu = {
	shown: function(){
		$('body').addClass('modal-open');
		$('#navbar').collapse('show');
		$('.app').append('<div class="modal-backdrop fade" href="#"></div>').add($('.modal-backdrop').addClass('in'));
		phoneSliderMob.slider.slick("getSlick").refresh();
	},
	hidden: function(){
		$('body').removeClass('modal-open');
		$('#navbar').collapse('hide');
		$('.modal-backdrop').css('display', 'none').remove();
	},
	init: function(){
		$(document).on('click.bs.collapse', '.navbar-top-toggle', touchMenu.shown);
		$(document).on('click.bs.collapse', '.app-header-nav .navbar-toggle', touchMenu.hidden);
		$(document).on('click', '.modal-backdrop', function(){
			if($('.collapse.in').is(':visible')){
				$('.app-header-nav .navbar-toggle').trigger('click');
			}
		});

		$('.main-navbar > li').hover(
			function (e) {
				if (!Utilities.mobile()) {
					if ($(this).children('.dropdown-menu').length) {
						$(this).addClass('open');
					}
				}
			},
			function () {
				if (!Utilities.mobile()) {
					$(this).removeClass('open');
				}
			}
		)
	}
};

var phoneSlider = {
	slider: $('.carousel-phone-list'),
	init: function(){
		if($('.carousel-phone-list').exists()){
			phoneSlider.slider.slick({
				slidesToScroll: 1,
				slidesToShow: 1,
				dots: false,
				arrows: true,
				infinite: false,
				speed: 400
			});
		}
	}
};

var phoneSliderMob = {
	slider: $('.carousel-phone-mob-list'),
	init: function(){
		if($('.carousel-phone-mob-list').exists()){
			slider = phoneSliderMob.slider.slick({
				slidesToScroll: 1,
				slidesToShow: 1,
				dots: false,
				arrows: true,
				infinite: false,
				speed: 400
			});
		}
	}
};

var mainSlider = {
	slider: $('.main-carousel-state'),
	sliderNav: $('.main-carousel-nav'),
	init: function(){
		if($('.main-carousel').exists()){
			slider = mainSlider.slider.on('beforeChange', function(event, slick, slide, nextSlide){
				mainSlider.sliderNav.find('.slick-slide').removeClass('slick-current').eq(nextSlide).addClass('slick-current');
			}).slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true,
				asNavFor: '.main-carousel-nav',
				focusOnSelect: false,
				arrows: false,
				speed: 500,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							prevArrow: $('.main-carousel-state-prev'),
							nextArrow: $('.main-carousel-state-next'),
							arrows: true
						}
					}
				]
			});
			sliderNav = mainSlider.sliderNav.slick({
				slidesToShow: 4,
				slidesToScroll: 1,
				asNavFor: '.main-carousel-state',
				infinite: true,
				dots: false,
				arrows: false,
				speed: 500,
				focusOnSelect: true
			});
		}
	}
};

var fullSlider = {
	slider: $('.full-carousel-state'),
	//sliderNav: $('.full-carousel-nav'),
	init: function(){
		if($('.full-carousel').exists()){
			slider = fullSlider.slider.on('beforeChange', function(event, slick, slide, nextSlide){
				fullSlider.sliderNav.find('.slick-slide').removeClass('slick-current').eq(nextSlide).addClass('slick-current');
			}).slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true,
				//asNavFor: '.full-carousel-nav',
				focusOnSelect: false,
				//prevArrow: $('.full-carousel-state-prev'),
				//nextArrow: $('.full-carousel-state-next'),
				fade: true,
				//arrows: true,
				arrows: false,
				dots: false,
				speed: 500,
				responsive: [
					{
						breakpoint: 1024,
						settings: {
							arrows: false,
							dots: true
						}
					}
				]
			});
			/*sliderNav = fullSlider.sliderNav.slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				asNavFor: '.full-carousel-state',
				infinite: false,
				dots: false,
				arrows: false,
				vertical: true,
				speed: 500,
				focusOnSelect: true
			});*/
		}
	}
};

var booksSlider = {
	slider: $('.books-slider'),
	sliderSettings: function(){
		return {
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: true,
			focusOnSelect: false,
			variableWidth: true,
			arrows: true,
			dots: false,
			speed: 500,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						dots: false
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						// centerMode: true,
						variableWidth: false,
						dots: true
					}
				}
			]
		}
	},
	init: function(){
		if($('.books-carousel').exists()){
			booksSlider.slider.slick(booksSlider.sliderSettings());
		}
	}
};

var listInlineHover = {
	slider: $('.list-inline-hover'),
	sliderSettings: function(){
		return {
			slidesToScroll: 1,
			slidesToShow: 1,
			dots: true,
			arrows: false,
			infinite: true,
			adaptiveHeight: true,
			speed: 500
		};
	},
	
	init: function(){
		if($('.list-inline-hover').exists() && $(window).width() < 1024){
			listInlineHover.slider.slick(listInlineHover.sliderSettings());
		}
		if (window.addEventListener) {
			window.addEventListener("resize", function() {
				if($(window).width() >= 1024){
					if(listInlineHover.slider.hasClass('slick-initialized')){
						listInlineHover.slider.slick('unslick');
					}
				} else {
					if(listInlineHover.slider.hasClass('slick-initialized')){
						listInlineHover.slider.slick('unslick');
					}
					listInlineHover.slider.slick(listInlineHover.sliderSettings());
				}
			}, false);
		} else if (window.attachEvent) {
			window.attachEvent("resize", function() {
				if($(window).width() >= 1024){
					if(listInlineHover.slider.hasClass('slick-initialized')){
						listInlineHover.slider.slick('unslick');
					}
				} else {
					if(listInlineHover.slider.hasClass('slick-initialized')){
						listInlineHover.slider.slick('unslick');
					}
					listInlineHover.slider.slick(listInlineHover.sliderSettings());
				}
			}, false);
		}
	}
};

var listInlineBanner = {
	slider: $('.list-inline-banner'),
	sliderSettings: function(){
		return {
			slidesToScroll: 1,
			slidesToShow: 1,
			dots: true,
			arrows: false,
			infinite: true,
			adaptiveHeight: true,
			speed: 500
		};
	},
	
	init: function(){
		if($('.list-inline-banner').exists() && $(window).width() < 1024){
			listInlineBanner.slider.slick(listInlineBanner.sliderSettings());
		}
		if (window.addEventListener) {
			window.addEventListener("resize", function() {
				if($(window).width() >= 1024){
					if(listInlineBanner.slider.hasClass('slick-initialized')){
						listInlineBanner.slider.slick('unslick');
					}
				} else {
					if(listInlineBanner.slider.hasClass('slick-initialized')){
						listInlineBanner.slider.slick('unslick');
					}
					listInlineBanner.slider.slick(listInlineBanner.sliderSettings());
				}
			}, false);
		} else if (window.attachEvent) {
			window.attachEvent("resize", function() {
				if($(window).width() >= 1024){
					if(listInlineBanner.slider.hasClass('slick-initialized')){
						listInlineBanner.slider.slick('unslick');
					}
				} else {
					if(listInlineBanner.slider.hasClass('slick-initialized')){
						listInlineBanner.slider.slick('unslick');
					}
					listInlineBanner.slider.slick(listInlineBanner.sliderSettings());
				}
			}, false);
		}
	}
};

Popup = {
	hideModal: function(id){
		var id = (id) ? id : '#popup-pdfviewer';
		var idForm = $(id).find('form');
		if($('.collapse.in').is(':visible')){
			$(id).collapse('hide');
			$('.modal-backdrop').fadeOut(function() {
				this.remove();
			});
			$(id).collapse('hide');
			$('body').removeClass('modal-open');
			if($(idForm).exists()){
				FormValFriend.clear('#' + $(idForm).attr('id'));
			}
		}
	},
	
	shownModal: function(id){
		var id = (id) ? id : '#popup-feedback';
		$('body').addClass('modal-open');
		if($('#navbar').is(':visible'))
			$('#navbar').collapse('hide');
		//$(id).collapse('show').before('<div class="modal-backdrop fade"></div>').add($('.modal-backdrop').css('background-color', '#ffffff').addClass('in'));
		$(id).collapse('show').before('<div class="modal-backdrop fade"></div>').add($('.modal-backdrop').css('background-color', '#ffffff').addClass('in').css('height', '110%'));
		if(/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
			$('.modal').addClass('modal_for_ios');
			$('.modal-backdrop').addClass('modal_for_ios');
		}
		
		$(document).on('click.bs.collapse', '.modal .close', function() {
			Popup.hideModal(id);
		});
	},
	
	shownModalFixed: function(id){
		var id = (id) ? id : '#popup-feedback';
		$('body').addClass('modal-open');
		if($('#navbar').is(':visible'))
			$('#navbar').collapse('hide');
		$(id).collapse('show').add($(id).children('.modal-dialog').css({
			"top": (($(window).height() - $(id).children('.modal-dialog').outerHeight()) / 2 < 10) ? '10px' : ($(window).height() - $(id).children('.modal-dialog').outerHeight()) / 2 + 'px',
			"left": '0',
			"right": '0',
			"position": 'absolute',
			"z-index": '1020'
		})).before('<div class="modal-backdrop fade"></div>').add($('.modal-backdrop').css('background-color', '#ffffff').addClass('in').css('height', '110%'));
		if(/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
			$('.modal').addClass('modal_for_ios');
			$('.modal-backdrop').addClass('modal_for_ios');
		}
		
		$(document).on('click.bs.collapse', '.modal .close', function() {
			Popup.hideModal(id);
		});
	},
	
	init: function(){
		$(document).on('click.bs.collapse', '.js-feedback', function(e) {
			var $this = $(this);
			if (!$this.attr('data-target'))
				e.preventDefault();
			Popup.shownModal('#popup-feedback');
		});

		$(document).on('click.bs.collapse', '.js-guid', function(e) {
			var $this = $(this);
			if (!$this.attr('data-target'))
				e.preventDefault();
			Popup.shownModal('#popup-guid');
		});
		$(document).on('click.bs.collapse', '.js-link-target', function(e) {
			var $this = $(this);
			if (!$this.attr('data-target'))
				e.preventDefault();
			$('#popup-target').find('.js-popup-target-text').text($this.attr('data-target').replace(/^.*?:\/\//,''));
			$('#popup-target').find('.js-popup-target-link').attr('href', $this.attr('data-target'));
			Popup.shownModal('#popup-target');
		});
	}
};

Tools = {
	findInUrlParam: function() {
		var result = undefined
				tmp = [];
		
		location.hash.substr(1).split("?").forEach(function (item, index) {
			tmp[index] = item;
			result = tmp[1];
		});
		return result;
	},
	
	setCookie: function (name, value, expires, path, domain, secure) {
		var curr_date = new Date();
		curr_date.setDate(curr_date.getDate() + 7);
		var curr_date_string = curr_date.toUTCString();
		document.cookie = name + "=" + escape(value) +
		((expires) ? "; expires=" + expires : "; expires=" + curr_date_string) +
		((path) ? "; path=" + path : "; path=/") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
	},
	getCookie: function (name) {
		var cookie = " " + document.cookie;
		var search = " " + name + "=";
		var setStr = null;
		var offset = 0;
		var end = 0;
		if (cookie.length > 0) {
			offset = cookie.indexOf(search);
			if (offset != -1) {
				offset += search.length;
				end = cookie.indexOf(";", offset)
				if (end == -1) {
					end = cookie.length;
				}
				setStr = unescape(cookie.substring(offset, end));
			}
		}
		return (setStr);
	},
	delCookie: function (name) {
		document.cookie = name + "=" + "; expires=Thu, 01 Jan 1970 00:00:01 GMT";
	},

	objectSize: function(the_object){
		var object_size = 0,
				key;
		for (key in the_object) {
			if (the_object.hasOwnProperty(key)) {
				object_size++;
			}
		}
		return object_size;
	}
}

scrollToId = {
	toId: function(id){
		$('html, body').animate({ scrollTop: parseInt($(id).offset().top) }, 1000);
	},
	
	init: function(){
		$('.js-scrollTo').on('click', function(e){
			var $this = $(e.currentTarget);
			var dataProd = $this.data('prod');
			if(dataProd !== ""){
				$("#prod_summary").val(dataProd).trigger("change");
			} else {
				$("#prod_summary").val("").trigger("change");
			}
			scrollToId.toId($this.attr('href'));
		});
	}
};

tabNav = {
	init: function(){
		if ($('.tab').exists()) {
			$('.tab-nav').each(function(){
				var handler = $(this).children(".tab-nav__item").eq(0);
				if(handler != 0){
					handler.children('.tab-nav-handler').tab('show');
				}
			});
			
			$('.tab-nav__item .tab-nav-handler').on('click', function(e) {
				var $this = $(e.currentTarget);
				if($this.attr("href")) e.preventDefault();
				$this.tab('show');
				if($this.parents('.tab-cover.dropdown').length){
					var $parents = $this.parents('.tab-cover.dropdown');
					var dataText = $this.data('text');
					$parents.find('.filter-option').empty().text(dataText);
				}
			});
			
			var masActiv = {'#tab-1':"false"};
			$('.tab-nav__item .tab-nav-handler').on('shown.bs.tab', function (e) {
				var index = $(e.target).attr('href');
				if(masActiv[index] != 'false'){
					/*if($('.slider').exists()){
						$('.slider:visible').each(function(){
							$(this).slick("getSlick").refresh();
						});
					}*/
					$('.js-fixed-height').box('refresh');
					masActiv[[index]] = 'false';
				}

				if (booksSlider.slider.hasClass('slick-initialized')) {
					if(booksSlider.slider.is(':visible')){
						booksSlider.slider.slick('unslick');
						booksSlider.slider.slick(booksSlider.sliderSettings());
					}
				}
			});
			
			$('.js-tab-open').on('click', function(e){
				var $this = $(e.currentTarget);
				if($this.attr("href")) e.preventDefault();
				$('.tab-nav-handler[href="' + $this.attr("href") + '"]').trigger('click');
			});
		}
	}
};

var scrollNav = {
	init: function(){
		var $nav = $('.sections-nav'),
				$body = $(document.body),
				offsetTopTmp;
		if($(window).width() >= 1024){
			offsetTopTmp = $(".section-promo").height() - $nav.height();
		} else {
			offsetTopTmp = $(".section-promo").height() + $nav.height();
		}

		$body.scrollspy({
			target: ".sections-nav",
			offset: 110
		});
		
		$('html, body').animate({ scrollTop: 0 }, 100, function(){
			$nav.find('.progress-bar').css('width', '0%');
			//console.log('hash= ', window.location.hash);
		});
		
		$(document).on('click', '.sections-nav li', function(e){
			var target = $(e.currentTarget).children().attr('href');
			$('html, body').stop().animate({ scrollTop: $(target).offset().top}, 800, function(){
				window.location.hash = target;
			});
		});

		$nav.on('activate activate.bs.scrollspy', function(e) {
			$body.scrollspy("refresh");
			var $active = $nav.find('li.active');
			$active.prevAll().addClass('complete').find('.progress-bar').css('width', '100%');
			$active.nextAll().removeClass('complete').find('.progress-bar').css('width', '0%');
		});

		$(window).on('scroll', function(event) {
			if($(this).scrollTop() != 0)
				$(".start").hide("slow");
			//if($(this).scrollTop() >= $(".section-promo").height()){
			if ($(this).scrollTop() >= offsetTopTmp) {
				$nav.addClass('fixed-top');
			} else {
				$nav.removeClass('fixed-top');
			}

			var $active = $nav.find('li.active'),
				$progress = $active.find('.progress-bar'),
				$scrollspy = $body.data('bs.scrollspy'),
				scrollTop = $scrollspy.$scrollElement.scrollTop() + $scrollspy.options.offset,
				scrollHeight = $scrollspy.$scrollElement[0].scrollHeight || $scrollspy.$body[0].scrollHeight,
				maxScroll = scrollHeight - $scrollspy.$scrollElement.height(),
				offsets = $scrollspy.offsets,
				targets = $scrollspy.targets,
				activeTarget = $scrollspy.activeTarget,
				i;

			if (scrollTop >= maxScroll) {
				$progress.css('width', '100%');
				return;
			}

			if (activeTarget && scrollTop <= offsets[0]) {
				$progress.css('width', '0%');
				return;
			}
			for (i = offsets.length; i--;) {
				if (scrollTop >= offsets[i] && (!offsets[i + 1] || scrollTop <= offsets[i + 1])) {
					var p1 = offsets[i],
						p2 = scrollTop,
						p3 = !offsets[i + 1] ? maxScroll : offsets[i + 1],
						p = Math.round((p2 - p1) / (p3 - p1) * 100);
					$progress.css('width', (p < 2 ? 2 : p) + '%');
					return;
				}
			}
		});

		$(window).on('resize', function() {
			if($(window).width() >= 1024) {
				offsetTopTmp = $(".section-promo").height() - $nav.height();
			} else {
				offsetTopTmp = $(".section-promo").height() + $nav.height();
			}
			//offsetTopTmp = $nav.offset().top + $nav.height();
		});
	}
};

$(function() {
	touchMenu.init();
	phoneSlider.init();
	phoneSliderMob.init();
	mainSlider.init();
	fullSlider.init();
	booksSlider.init();
	listInlineHover.init();
	listInlineBanner.init();
	scrollToId.init();
	tabNav.init();
	Popup.init();
	
	if ($('.sections-nav').exists()){
		scrollNav.init();
	}

	if ($('.main-navbar > li').exists()) {
		$(window).on('resize', function () {
			if ($(window).width() >= 1024) {
				$('.app-header-nav').removeClass('collapse').removeClass('in').removeAttr('style');
				$('.modal-backdrop.fade.in').css('display', 'none').remove();
			}
		});
	}

	if ($('.tab').exists()) {
		$('.tab-nav').each(function(){
			var handler = $(this).children(".tab-nav__item").eq(0);
			if(handler != 0){
				handler.children('.tab-nav-handler').tab('show');
			}
		});
		
		$('.tab-nav__item .tab-nav-handler').on('click', function(e) {
			var $this = $(e.currentTarget);
			if($this.attr("href")) e.preventDefault();
			$this.tab('show');
			if($this.parents('.tab-cover.dropdown').length){
				var $parents = $this.parents('.tab-cover.dropdown');
				var dataText = $this.data('text');
				$parents.find('.filter-option').empty().text(dataText);
			}
		});
		
		var masActiv = {'#tab-1':"false"};
		$('.tab-nav__item .tab-nav-handler').on('shown.bs.tab', function (e) {
			var index = $(e.target).attr('href');
			if(masActiv[index] != 'false'){
				if($('.rate-slider').exists()){
					$('.rate-slider:visible').each(function(){
						$(this).slick("getSlick").refresh();
					});
				}
				$('.js-fixed-height').box('refresh');
				masActiv[[index]] = 'false';
			}
		});
		
		$('.js-tab-open').on('click', function(e){
			var $this = $(e.currentTarget);
			if($this.attr("href")) e.preventDefault();
			$('.tab-nav-handler[href="' + $this.attr("href") + '"]').trigger('click');
		});
	}
	
	$(document).on('click', '.js-print', function(e){
		window.print();
	});
	
	$(window).load(function() {
		var hash = window.location.hash;
		if(hash.indexOf('#tab') + 1){
			$('.tab-nav-handler[href="#' + hash.substr(1).split("?")[0] + '"]').trigger('click');
		} else {
			(window.location.hash) ? scrollToElement(hash.substr(1).split("?")[0]) : $(window).scrollTop(0);
		}
	});
	
	if ($('.rules .scrollbar').exists()) {
		$('.rules .scrollbar').scroller({
			handleSize: 40
		});
	}

	if ($('.agreement .scrollbar').exists()) {
		$('.agreement .scrollbar').scroller({
			handleSize: 40
		});
	}
	
	if($('.js-fixed-height').exists() && $('.js-fixed-height').is(':visible')){
		$('.js-fixed-height').box();
	}
	
	if($('.card-mode').exists()){
		$('.card-mode-content .card-img-1, .card-mode-content .card-img-2, .card-mode-content .card-img-3, .card-mode-content .card-img-4, .card-mode-content .card-img-gold, .card-mode-content .card-img-priority').css('perspective', $('.card-mode-content').width());
		if (window.addEventListener) {
			window.addEventListener("resize", function() {
				$('.card-mode-content .card-img-1, .card-mode-content .card-img-2, .card-mode-content .card-img-3, .card-mode-content .card-img-4, .card-mode-content .card-img-gold, .card-mode-content .card-img-priority').css('perspective', $('.card-mode-content').width());
			}, false);
		} else if (window.attachEvent) {
			window.attachEvent("resize", function() {
				$('.card-mode-content .card-img-1, .card-mode-content .card-img-2, .card-mode-content .card-img-3, .card-mode-content .card-img-4, .card-mode-content .card-img-gold, .card-mode-content .card-img-priority').css('perspective', $('.card-mode-content').width());
			}, false);
		}
	}

	$(document).on('click', 'a[data-toggle="collapse"]', function (e) {
		var $this = $(e.currentTarget);
		var hasOpened = $this.attr('aria-expanded');
		var parentEl = $this.attr('data-parent');

		if (hasOpened) {
			$(parentEl).find('.js-fixed-height').box('refresh');
		}
	});

	// if($('#lang-switcher').exists()){
	// 	var path = window.location.pathname.split('/');
	// 	$('#lang-switcher').attr('href', $('#lang-switcher').attr('href')+path.pop());
	// }
});

function scrollToElement(className) {
	var el =  '.' + className;
	if ($(el).length > 0) {
		$('html, body').animate({ scrollTop: parseInt($(el).offset().top), scrollLeft: parseInt($(el).offset().left) }, 100);
	}
}