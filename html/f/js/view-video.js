'use strict';

var video = {
  player: null,
  stop: function(){
    var self = this;

    $('.preview__video-overlay, .preview__video-close').on('click', function(e){
      self.player.pause();
      self.overlay.hide();
      self.wrap.hide();
      $('body').removeClass('modal-open');
      e.stopPropagation();
    });
  },
  play: function(){
    var self = this;
  
    this.btnPlay.on('click', function(){
      self.overlay.show();
      self.wrap.show();
      $('body').addClass('modal-open');
      self.player.play();
    });
  },
  initPlayer: function(){
    this.player = videojs('js-preview-video',
    {
      controlBar: {
      volumeControl: false,
      muteToggle: false,
      fullscreenToggle: false
      }
    });
  },
  init: function(){
    this.overlay = $('.preview__video-overlay');
    this.btnPlay = $('.video-link');
    this.btnClose = $('.preview__video-close');
    this.wrap = $('.preview__video-wrap');

    if ($('#js-preview-video').length) {
      this.initPlayer();
      this.play();
      this.stop();
    }
  }
};

$(function() {
  video.init();
});
