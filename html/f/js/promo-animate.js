$(function(){
  $(window).on("load", function() {
      setTimeout(function() {
        $(".app-promo").addClass("hello_show1");
      }, 100);
      setTimeout(function() {
        $(".app-promo").addClass("hello_show2");
      }, 700);
      setTimeout(function() {
        $(".app-promo").addClass("hello_show3");
      }, 1200);
  });
  
  $(window).on("scroll", function() {
    var scrollTop = $(window).scrollTop();
    var windowHeight = $(window).height();
    var specialTop = $(".section-special").offset();
    var investTop = $(".section-investstrategy").offset();
    var cardsTop = $(".section-cards").offset();
    var cardsTopTwo = $(".section-cards-2").offset();
    
    function inWindow(s){
      var el = $(s);
      var offset = el.offset();
      if(scrollTop <= offset.top && (el.height() + offset.top) < (scrollTop + windowHeight)) {
        $(".section-about__media").addClass("scroll-effects");
      }
    }
    if($('.section-about__media').exists()){
      var boxesInWindow = inWindow(".section-about__media");
    }
    if($(".section-special").exists()){
      if(scrollTop >= specialTop.top + windowHeight / 8) {
        $(".particle-page-item").each(function(i) {
          $(this).delay((i++) * 150).animate({
            "opacity": "1",
            "top": "0"
          }, 600);
        });
      }
    }
    if($(".section-investstrategy").exists()){
      if(scrollTop >= investTop.top - windowHeight / 3) {
        $(".section-investstrategy .frame-caption").addClass("scroll-effects");
      }
    }
    if($(".section-cards").exists()){
      if(scrollTop >= cardsTop.top - windowHeight / 3) {
        $(".section-cards .card-mode").addClass("start-animation");
      }
    }
    if($(".section-cards-2").exists()){
      if(scrollTop >= cardsTopTwo.top - windowHeight / 3) {
        $(".section-cards-2 .card-mode").addClass("start-animation");
      }
    }
  });
});
