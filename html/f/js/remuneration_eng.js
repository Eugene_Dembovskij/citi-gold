$(function(){
  'use strict';
  window.app = $.extend(true, window.app || {}, {
    init: function() {
      $.each(this.module, function(index, module) {
        if (module.init) {
          module.init.apply(this);
          module.init = false;
        }
      });
    },
    module: {
      remuneration: {
        reflow: function() {
          var self;
          self = this;
          $('.remuneration__list').children().hide();
          $('.remuneration__list').children().eq(parseInt($('#remuneration_control').val(), 10)).show();
        },
        init: function() {
          var self;
          self = this;
          $("#remuneration_control").ionRangeSlider({
            values: [0, 1, 2, 3],
            grid: true,
            force_edges: true,
            grid_num: 2,
            hide_min_max: true,
            prettify: function(n) {
              var out;
              if (n === 0) {
                out = 'RUB 4,000,000-10,000,000';
              }
              if (n === 1) {
                out = 'RUB 10,000,000-20,000,000';
              }
              if (n === 2) {
                out = 'RUB 20,000,000-40,000,000';
              }
              if (n === 3) {
                out = '> RUB 40,000,000';
              }
              return out;
            },
            onChange: function() {
              return self.reflow();
            }
          });
        }
      }
    }
  });
  app.init();
});
