'use strict';

PDFJS.disableWorker = true;

var pdfDoc, pageNum, scale, canvas, ctx, viewerPrev, viewerNext, pager, pagerItem, viewerPopup, np;

function Init() {
  pdfDoc = null;
  pageNum = 1;
  scale = 1.5;
  canvas = document.getElementById('viewer-container');
  ctx = canvas.getContext('2d');
  pager = $('.viewer-pager');
  pagerItem = $('.viewer-pager li');
  viewerPrev = $('#viewer-prev');
  viewerNext = $('#viewer-next');
  viewerPopup = $('#viewer-popup');
  
  $(document).on('click', '#viewer-prev', goPrevious);
  $(document).on('click', '#viewer-next', goNext);
  $(document).on('click', '.viewer-pager li', goTo);
  $(document).on('click', '.pdf-link', goToLink);
  $('#popup-pdfviewer .viewer-close').on('click.bs.collapse', closePopup);
}

//
// Get page info from document, resize canvas accordingly, and render page
//
function renderPage(num) {
  pdfDoc.getPage(num).then(function (page) {
    var viewport = page.getViewport(scale);
    canvas.height = viewport.height;
    canvas.width = viewport.width;
    var renderContext = {
        canvasContext: ctx,
        viewport: viewport
    };
    page.render(renderContext);
  });
  //document.getElementById('page_num').textContent = pageNum;
  //document.getElementById('page_count').textContent = pdfDoc.numPages;
}

//
// Go to previous page
//
function goPrevious() {
  if ($(this).hasClass('disabled'))
    return;
  pageNum--;
  renderPage(pageNum);
  if(pageNum == 1)
    viewerPrev.addClass('disabled');
  viewerNext.removeClass('disabled');
  viewerPopup.collapse('hide');
  pager.find('.active').removeClass('active').end().children().eq(pageNum - 1).addClass('active');
}

//
// Go to next page
//
function goNext() {
  if ($(this).hasClass('disabled'))
    return;
  pageNum++;
  renderPage(pageNum);
  if(pageNum == pdfDoc.numPages){
    viewerNext.addClass('disabled');
    viewerPopup.collapse('show');
  }
  viewerPrev.removeClass('disabled');
  pager.find('.active').removeClass('active').end().children().eq(pageNum - 1).addClass('active');
}

function goTo(e){
  var $this = $(e.currentTarget);
  if(!$this.hasClass('active')){
    pageNum = $this.index() + 1;
    renderPage(pageNum);
    if(pageNum < pdfDoc.numPages){
      viewerNext.removeClass('disabled');
      viewerPopup.collapse('hide');
    } else {
      viewerNext.addClass('disabled');
      viewerPopup.collapse('show');
    }
    if(pageNum > 1){
      viewerPrev.removeClass('disabled');
    } else {
      viewerPrev.addClass('disabled');
    }
    $this.parent().find('.active').removeClass('active');
    $this.addClass('active');
  }
}

function loaderHide() {
  $('.loading').hide().remove();
}

function goToLink(e){
  var $this = $(e.currentTarget);
  var srcImg = $this.attr('data-src');
  var srcTmp = srcImg.substr(0, srcImg.lastIndexOf('/')) + '/thumb/';
  pageNum = 1;
  e.preventDefault();
  
  // if (!$this.attr('data-src')) e.preventDefault();
  $('body').addClass('modal-open');
  $('.app').prepend('<div class="loading"><div class="loading-icon">&nbsp;</div></div>');
  $('.loading').show();
  PDFJS.getDocument($this.attr('data-src')).then(function (doc) {
    pdfDoc = doc;
    np = (doc.numPages);
    
    pager.empty();
    for(var i = 1; i <= np; i++){
      pager.html(pager.html() + '<li class="thumbnail"><img src=' + srcTmp + i + '.jpg' + ' /></li>');
    }
    pager.children().eq(0).addClass('active');
    viewerPrev.addClass('disabled');
    viewerNext.removeClass('disabled');
    
    renderPage(pageNum);
    loaderHide();
    
    $('#popup-pdfviewer').collapse('show').before('<div class="modal-backdrop fade"></div>').add($('.modal-backdrop').css({'background-color': '#f3f1ee', 'opacity': '0.9'}).addClass('in'));
  });

}

function closePopup(){
  $('body').removeClass('modal-open');
  $('.modal-backdrop').fadeOut(function() {
    $(this).remove();
  });
  $('#popup-pdfviewer').collapse('hide');
  viewerPopup.collapse('hide');
}

$(function() {
  Init();
});
