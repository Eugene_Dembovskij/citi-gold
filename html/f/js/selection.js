var iewaParametrs = {
	id1: "The Ritz-Carlton, чайная церемония",
	id2: "ICON, пошив сорочки",
	id3: [
		"Aspire, билеты на сумму 7 700 руб",
		"Aspire, билеты на сумму 15 000 руб",
		"Aspire, билеты на сумму 25 000 руб",
		"Aspire, билеты на сумму 50 000 руб"
	],
	id4: "Cash 7 700 RUR",
	id5: "Cash 15 000 RUR",
	id6: "Cash 25 000 RUR",
	id7: "Cash 50 000 RUR",
	id8: "The Ritz-Carlton, уикэнд в Представительском люксе",
	id9: "The Ritz-Carlton, ночь в номере Superior",
	id10: "The Ritz-Carlton, spa процедура",
	id11: "The Ritz-Carlton, ночь в Представительском люксе",
	id12: "ICON, пошив костюма"
};
var parametrsId;
var parametrsClass;


$(document).ready(function() {
	var hrefParametrs = window.location.href.split("?")[1];
	parametrsId = hrefParametrs.split(";")[0].split('=')[1];
	parametrsClass = hrefParametrs.split(";")[1].split('=')[1];

	var block = '#lev' + parametrsClass;

	$(block).css('display', 'block');

	if (isSelected()) {
		$("button").attr("disabled", "disabled");
	}
});

var select = function(id) {
	if (id === 'id3') {
		message = iewaParametrs[id][parametrsClass-1]; 
	} else {
		message = iewaParametrs[id];
	}
	
	pushMessage(message);
}

function pushMessage(message) {
	var url = "https://uat.citi.com/securemail/last.sm?path=/russia/appforms/cg_mgm_bonus/russian/"
	
	if (location.hostname !== "www.citibank.ru") {
  		url.replace("www.citibank.com", "uat.citi.com");
  	}

  	Popup.shownModal('#popup-success');
  	blockButton();
	action(url);
}

function action(url) {
	$.post(url, {
		IEWAcategory: 'CG MGM Bonus choice',
		id: parametrsId,
		selection: message
	});
}

function blockButton() {
	$("button").attr("disabled", "disabled");
	$.cookie(parametrsId, 'true', {
	    expires: 2
	});
}

var isSelected = function() {
	if ($.cookie(parametrsId) == 'true') {
		return true;
	}
	return false;
}