$(function () {
  'use strict';

    window.app = $.extend(true, window.app || {}, {
        init: function() {
            $.each(this.module, function(index, module) {
                if (module.init) {
                    module.init.apply(this);
                    module.init = false;
                }
            });
        },

        module: {
            remuneration: {
                min: 0,
                max: 7,
                slider: $("#remuneration_control"),
                // marks: ['20 000 руб.', '50 000 руб.', '75 000 руб.', 'по 30 000 руб.'],
                marks: ['20 000', '40 000', '60 000', '80 000', '100 000', '120 000', '140 000', '50 000'],
                reflow: function() {
                    var self;
                    self = this;
                    $('.remuneration__list').children().hide();
                    $('.remuneration__list').children().eq(parseInt($('#remuneration_control').val(), 10)).show();
                },
                markPosition: function($slider) {
                    $slider.find('.mark-grid .mark-grid-text').each(function () {
                        var $this = $(this);

                        if ($this.index() == 0) {
                            // $this.css({'margin-left': left});
                        } else if ($this.index() == ($this.parent().children().length - 1)) {
                            $(this).css({'margin-left': -1 * $this.outerWidth(false) + 'px'});
                        } else {
                            $(this).css({'margin-left': -1 * ($this.outerWidth(false) / 2) + 'px'});
                        }
                    });
                },
                convertToPercent: function (num) {
                    var self;
                    self = this;
                    var percent = (num - self.min) / (self.max - self.min) * 100;
                    return percent;
                },
                addMarks: function ($slider) {
                    var self;
                    var html = '<div class="mark-grid">';
                    var left = 0;
                    var i;

                    self = this;
                    for (i = 0; i < self.marks.length; i++) {
                        left = self.convertToPercent(i);
                        if (i == 0) {
                            html += '<span class="mark-grid-text" style="left: ' + left + '%; margin-left:-0.767347%;">' + self.marks[i] + '</span>';
                        } else if (i == self.marks.length - 1) {
                            html += '<span class="mark-grid-text" style="left: ' + left + '%; margin-left:-6.6612%;">' + self.marks[i] + '</span>';
                        } else {
                            html += '<span class="mark-grid-text" style="left: ' + left + '%">' + self.marks[i] + '</span>';
                        }
                    }

                    html += '</div>';
                    
                    $slider.find('.irs-grid').append(html);
                },
                init: function() {
                    var self, slider;
                    self = this;

                    var rtime;
                    var timeout = false;
                    var delta = 300;

                    $(window).on('resize', function () {
                        rtime = new Date();
                        if (timeout === false) {
                            timeout = true;
                            setTimeout(reload, delta);
                        }
                    });

                    function reload(){
                        if (new Date() - rtime < delta) {
                            setTimeout(reload, delta);
                        } else {
                            timeout = false;
                            self.markPosition(slider)
                        }
                    };

                    self.slider.ionRangeSlider({
                        type: "single",
                        values: [0, 1, 2, 3, 4, 5, 6, 7],
                        grid: true,
                        grid_snap: true,
                        grid_num: 8,
                        force_edges: true,
                        hide_min_max: true,
                        prettify: function(n) {
                            var out;

                            if (n === 0) {
                                out = '1&nbsp;друг';
                            }
                            if (n === 1) {
                                out = '2&nbsp;друга';
                            }
                            if (n === 2) {
                                out = '3&nbsp;друга';
                            }
                            if (n === 3) {
                                out = '4&nbsp;друга';
                            }
                            if (n === 4) {
                                out = '5&nbsp;друзей';
                            }
                            if (n === 5) {
                                out = '6&nbsp;друзей';
                            }
                            if (n === 6) {
                                out = '7&nbsp;друзей';
                            }
                            if (n === 7) {
                                out = 'каждый последующий';
                            }
                            return out;
                        },
                        onStart: function (data) {
                            slider = data.slider;
                            self.addMarks(data.slider);
                            self.markPosition(data.slider);
                            self.reflow();
                        },
                        onChange: function() {
                            return self.reflow();
                        }
                    });
                }
            }
        }
    });
    app.init();
});
