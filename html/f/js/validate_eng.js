Form = {
	branches: null,
	init: function(){
		var phone = $('#cglead input[name="phonenum11"]');
		if (phone.exists()) {
			phone.mask('+9(999)999-99-99');
		}
		
		branches = {
				1 : [
					{
						"name": "Metropolis",
						"value": "'Metropolis' branch"
					},
					{
						"name": "Na Arbate",
						"value": "'Na Arbate' branch"
					},
					{
						"name": "Na Bolshoy Nikitskoy",
						"value": "'Na Bolshoy Nikitskoy' branch"
					},
					{
						"name": "Na Komsomolskom prospecte",
						"value": "'Na Komsomolskom prospecte' branch"
					},
					{
						"name": "Na Krasnoy Presne",
						"value": "'Na Krasnoy Presne' branch"
					},
					{
						"name": "Na Leninskom",
						"value": "'Na Leninskom' branch"
					},
					{
						"name": "Na Marksistskoy",
						"value": "'Na Marksistskoy' branch"
					},
					{
						"name": "Na Tverskoy-Yamskoy",
						"value": "'Na Tverskoy-Yamskoy' branch"
					},
					{
						"name": "Ostozhenka",
						"value": "'Ostozhenka' branch"
					},
					{
						"name": "Paveletskoe",
						"value": "'Paveletskoe' branch"
					}
				],
				2 : [
					{
						"name": "Bolshoy prospekt",
						"value": "'Bolshoy prospekt (spb)' branch"
					},
					{
						"name": "Moskovsky prospekt",
						"value": "'Moskovsky prospekt (spb)' branch"
					},
					{
						"name": "Nevskiy prospekt",
						"value": "'Nevskiy prospekt (spb)' branch"
					}
				],
				3 : [
					{
						"name": "Volgogradskiy",
						"value": "'Volgogradskiy (Volgograd)' branch"
					}
				],
				4 : [
					{
						"name": "Uralskiy",
						"value": "'Uralskiy (Ekaterinburg)' branch"
					},
				],
				5: [
					{
						"name": "Privolzhskiy",
						"value": "'Privolzhskiy (Nizhniy Novgorod)' branch"
					}
				],
				6: [
					{
						"name": "Donskoy",
						"value": "'Donskoy (Rostov-on-Don)' branch"
					}
				],
				7: [
					{
						"name": "Srednevolzhskiy",
						"value": "'Srednevolzhskiy (Samara)' branch"
					}
				],
				8: [
					{
						"name": "Bashkortostan",
						"value": "'Bashkortostan (Ufa)' branch"
					}
				]
			};
		
		if($('.select-box').exists()){
			$(".select-box").selecter({
				customClass: 'scroll-select',
				handleSize: 60,
				callback: selectCallback,
				mobile: true
			});
		}
		
		function selectCallback(val, index){
			if($(this).find('select').attr('name') == 'city'){
				if($(this).find('select').val() != ''){
					$('#branch').closest('.selecter').find('.selecter-options').scroller('destroy');
					$('#branch').empty().append($("<option selected='selected' value=''>Branch</option>")).val('');
					$.each(branches[index], function(key, text){
						$('#branch').append($("<option></option>").attr("value", text.value).text(text.name));
					});
					$("#branch").selecter("update");
					$('#branch').closest('.selecter').find('.selecter-options').scroller();
					$("#branch").selecter("enable");
				} else {
					$('#branch').empty().append($("<option selected='selected' value=''>Branch</option>")).val('');
					$("#branch").selecter("update");
					$('#branch').closest('.selecter').find('.selecter-options').scroller('destroy');
					$('#branch').closest('.selecter').find('.selecter-options').scroller();
					$("#branch").selecter("disable");
				}
			}
		}
	}
};

FormVal = {
	$Form: null,
	FocusValue: 1,
	params: {},
	validation: function(e) {
		var $this = $(e.currentTarget);
		var data = '';
		var isError = false;
		if (e.data.jForm) {
			FormVal.$Form = e.data.jForm;
		}
		var tmp, tmpName;
		if(!$('#cglead input[name="agree"]').prop('checked') && $this.hasClass('disabled')) return false;
		FormVal.$Form.find('.js-requere').each(function(){
			tmp = $(this);
			tmpName = tmp.attr('name');
			if(tmpName){
				FormVal.params[tmpName] = tmp.val().trim();
			}
			
			if(tmpName == 'name'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					if (tmp.val() == '	') {
						tmp.val() = '';
					}
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				if (tmp.val().length < 2) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}

				if (!Utilities.TextValidateWordsEng(tmp.val())) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'phonenum11'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'city'){
				if(tmp.val() == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'branch' && !(tmp.parent().hasClass('disabled'))){
				if(tmp.val() == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'comment'){
				/*if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}*/
				if(Utilities.trimSpace(tmp.val()) != ''){
					if (tmp.val().length < 2) {
						if (tmp.val() == '	') {
							tmp.val() = '';
						}
						
						tmp.parent().addClass('has-error');
						isError = true;
						return false;
					}
					if (!Utilities.textValidateAreaEng(tmp.val())) {
						tmp.parent().addClass('has-error');
						isError = true;
						return false;
					}
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'client'){
				if (!tmp.prop("checked")) {
					tmp.val('no customer');
					FormVal.params[tmpName] = tmp.val();
				} else {
					tmp.val('Citibank customer');
					FormVal.params[tmpName] = tmp.val();
				}
			}
			
			if(tmpName == 'agree'){
				if (!tmp.prop("checked")) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.val('1');
				tmp.parent().removeClass('has-error');
				isError = false;
			}
		});
		
		if(isError){
			FormVal.$Form.find('.has-error').each(function(){
				if($(this).hasClass('has-error')){
					$('html').animate({ scrollTop: parseInt($(this).find('.js-requere').offset().top)}, 'slow');
					return false;
				}
			});
		} else {

			var data = {Fname: FormVal.params.name, nameRec: FormVal.params.nameRec, card_number: FormVal.params.card_number, email: FormVal.params.email, tel_num_1: FormVal.params.phonenum11, city: FormVal.params.city, branches: FormVal.params.branch, client: FormVal.params.client, comment: FormVal.params.comment, sourcecode: $('#sourcecode').val(), IEWAcategory: $('#IEWAcategory').val()};

			var path = window.location.pathname.split("/");
			function eventSendForm() {
				var loc = encodeURIComponent(window.location.href);
				var ref = encodeURIComponent(document.referrer);
				var pathLoc = loc.split("%2F");
				var matches = pathLoc[pathLoc.length - 1].substr(0, pathLoc[pathLoc.length - 1].search(/\.htm/i));
				var img = document.createElement('img');

				img.width = 1;
				img.height = 1;
				img.style.display = 'none';
				if (matches === 'servis-i-privilegii') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=prcall&__location=' + loc + '&__referrer=' + ref;
				}
				if (matches === 'privileges') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=castep_0&__location=' + loc + '&__referrer=' + ref;
				}
				if (matches === 'citigold_client') {
					img.src = '//tag.rutarget.ru/tag?event=castep_1&__location=' + loc + '&__referrer=' + ref;
				}
				img.alt = '';
				document.body.insertBefore(img, document.body.firstChild);
			}

			function eventSuccessForm() {
				var loc = encodeURIComponent(window.location.href);
				var ref = encodeURIComponent(document.referrer);
				var pathLoc = ref.split("%2F");
				var matches = pathLoc[pathLoc.length - 1].substr(0, pathLoc[pathLoc.length - 1].search(/\.htm/i));
				var img = document.createElement('img');

				img.width = 1;
				img.height = 1;
				img.style.display = 'none';
				if (matches === 'servis-i-privilegii') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=order_consult&__location=' + loc + '&__referrer=' + ref;
				}
				
				if (matches === 'privileges') {
					img.src = '//tag.rutarget.ru/tag?event=thankYou&conv_id=order_card&__location=' + loc + '&__referrer=' + ref;
				}
				img.alt = '';
				document.body.insertBefore(img, document.body.firstChild);
			}
			eventSendForm();
			
			//console.log('data= ', data);
			$.post(FormVal.$Form.attr('action'), data, function(json){
				//if(json.success){}
			}, "json");
			Popup.shownModal('#popup-success');
			if(path.length > 1){
				//var matches = path[path.length - 1].match(/\b\w+?\b/);
				//analitics('event13', matches[0]);
				
				analitics('event13', ';Citigold', '1464333941135', '1464333941135');
				yaCounter10209925.reachGoal('Form_citigold_client');
				eventSuccessForm();
			}
			//$('#popup-feedback .close').trigger('click');
			FormVal.clear(FormVal.$Form);
			
			ga('send', 'event', 'Click', 'Form', 'citigold_client');
			// window.location.href = "https://www.citibank.ru/russia/main/eng/citigold_client_thankyou.html";
		}
	},
	
	clear: function(idForm){
		$(idForm).find('.has-error').removeClass('has-error');
		$(idForm).find('.error-text').empty().hide().html('<li></li>');
		$(idForm)[0].reset();
		$('#city').val('').trigger('change');
		$('#submit').addClass('disabled');
	},
	
	init: function() {
		$('.form-field.required .field-email').on('blur', function(e) {
			var regexp = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
			if (regexp.test(this.value) !== true) {
				$(this).parent().addClass('field_error');
			} else {
				$(this).parent().removeClass('field_error');
			}
		});

		$('.form-field.required .field-text').on('keyup', function(e) {
			var regexp = /^[a-zA-Zа-яА-Я@]+$/;
			if (regexp.test(this.value) !== true) {
				this.value = this.value.replace(/[^a-zA-Zа-яА-Я@]+/, '');
			}
		});
		
		$('#cglead input[name="agree"]').on('change', function(){
			$('#submit').toggleClass('disabled');
		});

		
		$('#submit').on('click', { jForm: $('#cglead')}, FormVal.validation);
		/*$('#submit').on('click', function(){
			$('#popup-feedback .close').trigger('click');
			Popup.shownModal('#popup-success');
		});*/
	}
};

FormValFriend = {
	$Form: null,
	FocusValue: 1,
	params: {},
	validation: function(e) {
		var $this = $(e.currentTarget);
		var data = '';
		var isError = false;
		if (e.data.jForm) {
			FormValFriend.$Form = e.data.jForm;
		}
		var tmp, tmpName;
		if(!$('#cgmgm input[name="agree_1"]').prop('checked') && !$('#cgmgm input[name="agree_2"]').prop('checked') && $this.hasClass('disabled')) return false;
		FormValFriend.$Form.find('.js-requere').each(function(){
			tmp = $(this);
			tmpName = tmp.attr('name');
			if(tmpName){
				FormValFriend.params[tmpName] = tmp.val().trim();
			}
			
			if(tmpName == 'name'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					if (tmp.val() == '	') {
						tmp.val() = '';
					}
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				if (tmp.val().length < 2) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}

				if (!Utilities.TextValidateWordsEng(tmp.val())) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'card_number'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'nameRec'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					if (tmp.val() == '	') {
						tmp.val() = '';
					}
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				if (tmp.val().length < 2) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'email'){
				var regexp = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
				if (!Utilities.trimSpace(tmp.val()) == '' && regexp.test(tmp.val()) !== true) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'phonenum11'){
				if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'comment'){
				/*if(Utilities.trimSpace(tmp.val()) == ''){
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}*/
				if(Utilities.trimSpace(tmp.val()) != ''){
					if (tmp.val().length < 2) {
						if (tmp.val() == '	') {
							tmp.val() = '';
						}
						
						tmp.parent().addClass('has-error');
						isError = true;
						return false;
					}
					if (!Utilities.textValidateAreaEng(tmp.val())) {
						tmp.parent().addClass('has-error');
						isError = true;
						return false;
					}
				}
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'agree_1'){
				if (!tmp.prop("checked")) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.val('1');
				tmp.parent().removeClass('has-error');
				isError = false;
			}
			
			if(tmpName == 'agree_2'){
				if (!tmp.prop("checked")) {
					tmp.parent().addClass('has-error');
					isError = true;
					return false;
				}
				tmp.val('1');
				tmp.parent().removeClass('has-error');
				isError = false;
			}
		});
		
		if(isError){
			FormValFriend.$Form.find('.has-error').each(function(){
				if($(this).hasClass('has-error')){
					$('.modal.fade.in').animate({ scrollTop: parseInt($(this).find('.js-requere').offset().top) - parseInt($(this).parents('.modal-dialog').offset().top) }, 'slow');
					return false;
				}
			});
		} else {
			var data = {name: FormValFriend.params.name, nameRec: FormValFriend.params.nameRec, card_number: FormValFriend.params.card_number, email: FormValFriend.params.email, tel_num_1: FormValFriend.params.phonenum11, city: FormValFriend.params.city, branches: FormValFriend.params.branch, client: FormValFriend.params.client, comment: FormValFriend.params.comment, sourcecode: $('#sourcecode').val(), IEWAcategory: $('#IEWAcategory').val()};
			var path = window.location.pathname.split("/");
			
			//console.log('data= ', data);
			$.post(FormValFriend.$Form.attr('action'), data, function(json){
				//if(json.success){}
			}, "json");
			Popup.shownModal('#popup-guid-success');
			if(path.length > 1){
				//var matches = path[path.length - 1].match(/\b\w+?\b/);
				//analitics('event13', matches[0]);
				analitics('event13', ';Citigold', '1464333941135', '1464333941135');
				yaCounter10209925.reachGoal('Specials_Recommend_a_friend');
				ga('send', 'event', 'Click', 'Form', 'Specials_Recommend_a_friend');
			}
			$('#popup-guid .close').trigger('click');
			FormValFriend.clear(FormValFriend.$Form);
		}
	},
	
	clear: function(idForm){
		$(idForm).find('.has-error').removeClass('has-error');
		$(idForm).find('.error-text').empty().hide().html('<li></li>');
		$(idForm)[0].reset();
		$('#submit-rec').addClass('disabled');
	},
	
	init: function() {
		var cardNumber = $('#cgmgm input[name="card_number"]');
		var phone = $('#cgmgm input[name="phonenum11"]');
		if (cardNumber.exists()) {
			cardNumber.mask('9999 9999 9999 9999');
		}
		if (phone.exists()) {
			phone.mask('+9(999)999-99-99');
		}
		
		$('.field-email.js-requere').on('blur', function(e) {
			var regexp = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
			if (!Utilities.trimSpace(this.value) == '' && regexp.test(this.value) !== true) {
				$(this).parent().addClass('has-error');
			} else {
				$(this).parent().removeClass('has-error');
			}
		});

		$('.form-field.required .field-text').on('keyup', function(e) {
			var regexp = /^[a-zA-Zа-яА-Я@]+$/;
			if (regexp.test(this.value) !== true) {
				this.value = this.value.replace(/[^a-zA-Zа-яА-Я@]+/, '');
			}
		});
		
		$('#cgmgm input[name="agree_1"], #cgmgm input[name="agree_2"]').on('change', function(){
			$('#submit-rec').addClass('disabled');
			if($('#cgmgm input[name="agree_1"]').prop('checked') && $('#cgmgm input[name="agree_2"]').prop('checked')){
				$('#submit-rec').removeClass('disabled');
			}
		});

		$('#submit-rec').on('click', { jForm: $('#cgmgm')}, FormValFriend.validation);
	}
};

$(function() {
	Form.init();
	FormVal.init();
	FormValFriend.init();
});
